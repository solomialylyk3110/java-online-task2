package solomia;

import java.util.Scanner;

public class Application {
    public static int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }

    public static void main(String[] args) {
        final int attempts = 39;
        Scanner sc = new Scanner(System.in);
        int n;
        for(;;) {
            System.out.println("Enter number N of guests: ");
            n = sc.nextInt();
            if (n<=2) {
                System.out.println("Not enough guests! ");
            } else {
                break;
            }
        }
        int count = 0;
        int peopleHeardRumor = 0;
        for (int i = 0; i < attempts; i++) {
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean heard = false;
            int next;
            int current = 1;
            while (!heard) {
                next = 1 + (int) (Math.random() * (n - 1));
                if (next == current) {
                    while (next == current)
                        next = 1 + (int) (Math.random() * (n - 1));
                }
                if (guests[next]) {
                    if (rumorSpreaded(guests))
                        count++;
                    peopleHeardRumor = peopleHeardRumor + countPeopleReached(guests);
                    heard = true;
                }
                guests[next] = true;
                current = next;
            }
        }
        System.out.println("Probability that everyone will hear rumor except Alice in " + attempts + " attempts: " +
                (double) count / attempts);
        System.out.println("Average amount of people that rumor reached is: " + peopleHeardRumor / attempts);
    }

}
